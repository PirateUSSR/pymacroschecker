from functions import *
import openpyxl
import os
import rarfile
from shutil import *

name_list_path = "C:\\Users\\01551875\\Desktop\\DavydovFolder\\PyProjects\\macroschecker\\Data\\FullDescriptionFile.xlsx"
description = "C:\\Users\\01551875\\Desktop\\DavydovFolder\\PyProjects\\macroschecker\\Data" + '\\description_clear.xlsx'
ekP_all_files_dir_path = "C:\\Users\\01551875\\Desktop\\DavydovFolder\\PyProjects\\macroschecker\\Data\\FilesForSignFolder\\ЕКП"
ekS_all_files_dir_path = "C:\\Users\\01551875\\Desktop\\DavydovFolder\\PyProjects\\macroschecker\\Data\\FilesForSignFolder\\ЕКC"
ekP_package_path = "C:\\Users\\01551875\\Desktop\\DavydovFolder\\PyProjects\\macroschecker\\Data\\newPackagesForSignFolder\\ЕКП"
ekS_package_path = "C:\\Users\\01551875\\Desktop\\DavydovFolder\\PyProjects\\macroschecker\\Data\\newPackagesForSignFolder\\ЕКС"

filename_list = read_column_xlsx(name_list_path, 'A', 8,'name')
descname_list = read_column_xlsx(name_list_path, 'A', 8, 'description')
fullname_list = [filename_list, descname_list]


# Создание пакетов
def package_creating(all_files_dir_path, fullname_list, package_path, description_file):
    dir_tree = os.walk(all_files_dir_path)

    for dir in dir_tree:
        existFile = False

        filepath = dir[0]
        # print(filepath)
        for file in dir[2]:

            for name in fullname_list[0]:
                i = fullname_list[0].index(name)
                macrosDescription = fullname_list[1][i]
                if str(file).lower() == str(name).lower() and str(file) != "Paths.txt":
                    existFile = True
                    # print("Путь к файлу: "+filepath)
                    # print("Имя файла: "+str(file))
                    # print("Описание из файла: "+fullname_list[1][i])
                    if fullname_list[1][i] == "":
                        print("Описание у файла"+file+" отсутсвует")
                        macrosDescription = "Шаблон кредитного договора"
                    filesize = str(os.path.getsize(filepath+"\\"+str(file)))
                    os.mkdir(package_path+"\\"+str(file)+"."+filesize)  #  Создаем директорию в packages
                    copyfile(filepath+"\\"+str(file), package_path+"\\"+str(file)+"."+filesize+"\\"+str(file))
                    rename_file(package_path+"\\"+str(file)+"."+filesize+"\\", str(file))
                    copyfile(description_file, package_path+"\\"+str(file)+"."+filesize+"\\description.xlsx")
                    write_xlsx(package_path+"\\"+str(file)+"."+filesize+"\\description.xlsx", 'B3', str(file)+"."+filesize)
                    write_xlsx(package_path+"\\"+str(file)+"."+filesize+"\\description.xlsx", 'H8', macrosDescription)

            if existFile == False and str(file) != "Paths.txt":
                print("File "+str(file)+" был скопирован с универсальным описанием!")
                filesize = str(os.path.getsize(filepath + "\\" + str(file)))
                os.mkdir(package_path + "\\" + str(file) + "." + filesize)  # Создаем директорию в packages
                copyfile(filepath + "\\" + str(file), package_path + "\\" + str(file) + "." + filesize + "\\" + str(file))
                rename_file(package_path + "\\" + str(file) + "." + filesize + "\\", str(file))
                copyfile(description_file, package_path + "\\" + str(file) + "." + filesize + "\\description.xlsx")
                write_xlsx(package_path + "\\" + str(file) + "." + filesize + "\\description.xlsx", 'B3',
                           str(file) + "." + filesize)
                write_xlsx(package_path + "\\" + str(file) + "." + filesize + "\\description.xlsx", 'H8',
                           "Описание макроса")




# переименовывание форматов
def rename_extantion(path):

    file_list = os.walk(path)
    files_tree_list = []

    for file in file_list:
        files_tree_list.append(file)

    for filename in files_tree_list:

        print("cd " + filename[0])
        print("\"C:\Program Files\WinRAR\WinRar.exe\" a package.rar")
        for file in filename[2]:
            name, file_extension = os.path.splitext(file)
            # # print(file_extension)
            # # if file_extension == ".rar":
            # #     print(filename[0] + "\\" + file)
            if name != "description":
                if file_extension.lower() == '.xlt' or file_extension.lower() == '.xlsm' or file_extension.lower() == '.xltm' or file_extension.lower() == '.xlsx':
                    #     # print(name+file_extension)
                    #     # print(filename[0] + "\\" + file)
                    #     # print("Extention == "+file_extension)
                    os.rename(filename[0] + "\\" + file, filename[0] + "\\" + name + ".xls")
                # # print("Renamed from " + filename[0] + "\\" + file +" IN "+ filename[0] + "\\" + name + ".xls")
                #
                elif file_extension.lower() == '.dot':
                    #     # print("Extention = "+file_extension)
                    os.rename(filename[0] + "\\" + file, filename[0] + "\\" + name + ".doc")



# package_creating(ekP_all_files_dir_path, fullname_list, ekP_package_path, description)
# package_creating(ekS_all_files_dir_path, fullname_list, ekS_package_path, description)
# rename_extantion(ekP_package_path+"\\")
# rename_extantion(ekS_package_path+"\\")


# Распаковка пакетов
# 1) Извлеч номера директорий из .msg (VBS)
# 2) Скопировать .msg из \\Gerta1.ca.sbrf.ru\vol5\MACROS\out\<номера директори>\<номера директорий>.msg

num_dir_list_path_EKP = "C:\\Users\\01551875\\Desktop\\DavydovFolder\\PyProjects\\macroschecker\\Data\\num_dir_list_ekp.xlsx"
num_dir_list_path_EKS = "C:\\Users\\01551875\\Desktop\\DavydovFolder\\PyProjects\\macroschecker\\Data\\num_dir_list_eks.xlsx"
signed_packages_path = "\\\\Gerta1.ca.sbrf.ru\\vol5\\MACROS\\out\\"
local_signed_pack_path = "C:\\Users\\01551875\\Desktop\\DavydovFolder\\PyProjects\\macroschecker\\Data\\SygnedMacrosPackages\\"
unpacked_signed_macros_path_ekp = "C:\\Users\\01551875\\Desktop\\DavydovFolder\\PyProjects\\macroschecker\\Data\\UnpackedSignedMacros\\ЕКП\\"
unpacked_signed_macros_path_eks = "C:\\Users\\01551875\\Desktop\\DavydovFolder\\PyProjects\\macroschecker\\Data\\UnpackedSignedMacros\\ЕКС\\"
dir_list_ekp = read_column_xlsx(num_dir_list_path_EKP, 'A', 1, 'name')
dir_list_eks = read_column_xlsx(num_dir_list_path_EKS, 'A', 1, 'name')


def unpack_macros(signed_packages_path, local_signed_pack_path, unpacked_signed_macros_path, dir_list):
    for dirname in dir_list:
        dirname = str(dirname)
        os.mkdir(local_signed_pack_path + dirname)
        copy(signed_packages_path + dirname + "\\" + dirname + ".msg",
             local_signed_pack_path + dirname + "\\" + dirname + ".zip")  # скопировать и поменять расширение
        if os.path.exists(signed_packages_path + dirname + "\\" + dirname + ".xls"):
            copy(signed_packages_path + dirname + "\\" + dirname + ".xls",
                 local_signed_pack_path + dirname + "\\" + dirname + ".xls")  # скопировать и поменять расширение
        elif os.path.exists(signed_packages_path + dirname + "\\" + dirname + ".doc"):
            copy(signed_packages_path + dirname + "\\" + dirname + ".doc",
                 local_signed_pack_path + dirname + "\\" + dirname + ".doc")
        else:
            print("no .xls or .doc finded in dir: " + signed_packages_path + dirname + "\\" + dirname)

        print(
            "\"C:\\Program Files\\WinRAR\\WinRar.exe\" x -r \"C:\\Users\\01551875\\Desktop\\DavydovFolder\\PyProjects\\macroschecker\\Data\\SygnedMacrosPackages\\" +
            dirname + "\\" + dirname + ".zip\" -o \"C:\\Users\\01551875\\Desktop\\DavydovFolder\\PyProjects\\macroschecker\\Data\\SygnedMacrosPackages\\" + dirname + "\\")  # вывести текст распаковки для Unzip.cmd

        # if os.path.exists(local_signed_pack_path + dirname + "\\description.xls"):
        #     os.rename(local_signed_pack_path + dirname + "\\description.xls",
        #               local_signed_pack_path + dirname + "\\description.xlsx")
        # else:
        #     macros_name = read_cell_xlsx(local_signed_pack_path + dirname + "\\description.xlsx",
        #                                  'B3')  # Извлекаем имена макросов
        #
        # # Переименовываем подписанные макросы<номер> на настоящие имена макросов
        # if os.path.exists(local_signed_pack_path + dirname + "\\" + dirname + ".xls"):
        #     os.rename(local_signed_pack_path + dirname + "\\" + dirname + ".xls",
        #               local_signed_pack_path + dirname + "\\" + macros_name)
        # elif os.path.exists(local_signed_pack_path + dirname + "\\" + dirname + ".doc"):
        #     os.rename(local_signed_pack_path + dirname + "\\" + dirname + ".doc",
        #               local_signed_pack_path + dirname + "\\" + macros_name)
        # else:
        #     print("forsign.xls NOT Found")
        #
        # if os.path.exists(local_signed_pack_path + dirname + "\\" + macros_name):
        #     if os.path.exists(unpacked_signed_macros_path + "\\" + macros_name):
        #         print("The path " + unpacked_signed_macros_path + "\\" + macros_name + " exist")
        #     else:
        #         os.mkdir(unpacked_signed_macros_path + "\\" + macros_name)  # Копируем подписанный макрос
        #         list = macros_name.split('.')
        #         newName = '.'.join(list[:-1])
        #         copyfile(local_signed_pack_path + dirname + "\\" + macros_name,
        #                  unpacked_signed_macros_path + "\\" + macros_name + '\\' + newName)
        #         print("The file " + newName + " was copied")
        # else:
        #     print("File " + macros_name + " NOT EXIST")

unpack_macros(signed_packages_path, local_signed_pack_path, unpacked_signed_macros_path_ekp, dir_list_ekp)
unpack_macros(signed_packages_path, local_signed_pack_path, unpacked_signed_macros_path_eks, dir_list_eks)



# создаем директории по именам файлов(имя файла+размер) и переименовывем файлы на имена без размеров
# file_list = os.walk(unpacked_signed_macros_path)
#
# files_tree_list = []
#
# for file in file_list:
#     files_tree_list.append(file)
#
# for filename in files_tree_list[0][2]:
#     # print(filename)
#     os.mkdir(unpacked_signed_macros_path+'\\dir\\'+filename)
#     list = filename.split('.')
#     newName = '.'.join(list[:-1])
#     copyfile(unpacked_signed_macros_path+'\\'+filename, unpacked_signed_macros_path+'\\dir\\'+filename+'\\'+newName)
#     print(newName)

# Возвращаем Paths.txt

# ekp_dirWithPaths = ekS_all_files_dir_path
# ekp_dirWithSignedMacroses = unpacked_signed_macros_path+'\\dir\\'
#
# file_list1 = os.walk(ekp_dirWithPaths)
# files_tree_list1 = []
# file_list2 = os.walk(ekp_dirWithSignedMacroses)
# files_tree_list2 = []
#
# for file1 in file_list1:
#     files_tree_list1.append(file1)
#
# for file2 in file_list2:
#     files_tree_list2.append(file2)
#
# print(files_tree_list1[0][1])
# print(files_tree_list2[0][1])
#
# for dirname1 in files_tree_list1[0][1]:
#
#     existfile = False
#     for dirname2 in files_tree_list2[0][1]:
#
#         if dirname1 == dirname2:
#             # print(dirname1+" exist")
#             existfile = True
#             copyfile(ekp_dirWithPaths+'\\'+dirname1+'\\Paths.txt', ekp_dirWithSignedMacroses+'\\'+dirname2+'\\Paths.txt')
#
#     if existfile == False:
#         print(dirname1+" NOT exist!!!")
#
#
# path = "C:\\Users\\01551875\\Desktop\\DavydovFolder\\PyProjects\\macroschecker\\Data\\Главный Эталон\\Неподписанный\\ЕКП\\"
#
# fw = os.walk(path)
# tree = []
#
# for file in fw:
#     # print(file[2])
#     tree.append(file[2])
#
#
#
# for filename in tree[0]:
#     os.mkdir(path+"newEKP\\"+filename+str(os.path.getsize(path+filename)))
#     copyfile(path+filename, path+"newEKP\\"+filename+str(os.path.getsize(path+filename))+"\\"+filename)
#
#
#Копирование файлов на подпись из файла
# filePathEKP = "C:\\Users\\01551875\\Desktop\\DavydovFolder\\PyProjects\\macroschecker\\Data\\PathForSignEKP.txt"
# filePathEKS = "C:\\Users\\01551875\\Desktop\\DavydovFolder\\PyProjects\\macroschecker\\Data\\PathForSignEKS.txt"
# pathEtalonEKP = open(filePathEKP)
# pathEtalonEKS = open(filePathEKS)
# pathTargetEKP = "C:\\Users\\01551875\\Desktop\\DavydovFolder\\PyProjects\\macroschecker\\Data\\FilesForSignFolder\\ЕКП\\"
# pathTargetEKS = "C:\\Users\\01551875\\Desktop\\DavydovFolder\\PyProjects\\macroschecker\\Data\\FilesForSignFolder\\ЕКC\\"
#
#
# copyFilesForSignFromFile(pathEtalonEKP, pathTargetEKP)
# copyFilesForSignFromFile(pathEtalonEKS, pathTargetEKS)







