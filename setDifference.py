import functions
import shutil
import os
ekP_etalon_dir_path = "C:\\Users\\01551875\\Desktop\\DavydovFolder\\PyProjects\\macroschecker\\Data\\EtalonUnsignetMacrosFolder\\ЕКП"
ekS_etalon_dir_path = "C:\\Users\\01551875\\Desktop\\DavydovFolder\\PyProjects\\macroschecker\\Data\\EtalonUnsignetMacrosFolder\\ЕКС"
#
ekP_N_path = "C:\\Users\\01551875\\Desktop\\DavydovFolder\\JavaProjects\\Data\\MacrosFromN\\New_reports"
ekS_N_path = "C:\\Users\\01551875\\Desktop\\DavydovFolder\\JavaProjects\\Data\\MacrosFromN\\New_reports_mb"
#
ekP_packages_for_sign_path = "C:\\Users\\01551875\\Desktop\\DavydovFolder\\PyProjects\\macroschecker\\Data\\FilesForSignFolder\\ЕКП"
ekS_packages_for_sign_path = "C:\\Users\\01551875\\Desktop\\DavydovFolder\\PyProjects\\macroschecker\\Data\\FilesForSignFolder\\ЕКС"

# test paths
etalon_dir = "C:\\Users\\01551875\\Desktop\\DavydovFolder\\PyProjects\\macroschecker\\Data\\testFolder\\EtalonDir"
N_dir = "C:\\Users\\01551875\\Desktop\\DavydovFolder\\PyProjects\\macroschecker\\Data\\testFolder\\NDir"
packages_for_sign_dir = "C:\\Users\\01551875\\Desktop\\DavydovFolder\\PyProjects\\macroschecker\\Data\\testFolder\\PackagesForSignDir"



# Рекурсивный просмотр директории и сохранение в массив имен всех фалов в директории
def get_diff_files(path, packagepath, searchpath):
    dir_tree = os.walk(path)
    list_of_files = []

    for name in dir_tree:
        # print(name[0]) #Путь файла
        filepath = name[0]

        for filename in name[2]:
            # print(filename)  # Имя файла
            # print(os.path.getsize(name[0]+"\\"+filename))
            filesize = os.path.getsize(name[0]+"\\"+filename)  # Получаем размер файла
            # os.mkdir(path+"\\"+str(filename)+ "." + str(filesize))  # Создаем директорию <Имя файла + размер файла>
            # print("Created dir: "+ path+"\\"+str(filename)+ "." + str(filesize))
            # shutil.move(path+"\\"+filename, path+"\\"+str(filename)+ "." + str(filesize)+"\\"+str(filename))  # Копируем файл в созданую директорию
            # print("The file"+filename+" was copied")
            find_diff_files_in_dir(path, str(filepath), str(filename), filesize, packagepath, searchpath)


def find_diff_files_in_dir(etalonpath, filepath, filename, etalonfilesize, packagepath, searchpath):
    dir_tree = os.walk(searchpath)
    # print(filepath)
    existfile = False
    for name in dir_tree:
        # print(name[0])
        for fname in name[2]:
            searchfilesize = os.path.getsize(str(name[0])+"\\"+str(fname))

            if filename.lower() == str(fname).lower():
                existfile = True
                if etalonfilesize == searchfilesize:
                    # print("File "+filename+" wos faund in : "+ name[0])
                    write_path_to_file(name[0], filepath)
                else:  # Найден неподписанный файл!
                    # print("File " + name[0]+"\\"+filename + " is different by size")
                    package_file_fun(filename, str(name[0]), searchfilesize, packagepath, etalonpath)

    if existfile == False:
        print("The file "+filename+ " was not founded in directory")
        reportpath = "C:\\Users\\01551875\\Desktop\\DavydovFolder\\PyProjects\\macroschecker\\Data\\EtalonUnsignetMacrosFolder"
        write_path_to_file("File "+filename+" was not founded", reportpath)



def package_file_fun(filename, filepath, filesize, packagepath, etalonpath):
    # print("The file "+filepath+"\\"+filename+" will be packed into "+packagepath+"\\"+filename+str(filesize))
    if os.path.exists(packagepath+"\\"+filename + "." + str(filesize)):
        write_path_to_file(filepath, packagepath + "\\" + filename + "." + str(filesize))
    else:
        os.mkdir(packagepath+"\\"+filename+"."+str(filesize))
        shutil.copyfile(filepath+"\\"+filename, packagepath+"\\"+filename+"."+str(filesize)+"\\"+filename)
        write_path_to_file(filepath, packagepath + "\\" + filename + "."+str(filesize))


def write_path_to_file(filepath, reportpath):
    f = open(reportpath+"\\"+"Paths.txt", 'a')
    f.write("\r\n"+filepath)

# get_diff_files(ekP_etalon_dir_path, ekP_packages_for_sign_path, ekP_N_path)
# get_diff_files(ekS_etalon_dir_path, ekS_packages_for_sign_path, ekS_N_path)
# get_diff_files(etalon_dir, packages_for_sign_dir, N_dir)