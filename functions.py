import shutil

import openpyxl
import os
import rarfile


def read_column_xlsx(file_path, column_name_of_reading, start_reading_from, type):
    wb = openpyxl.load_workbook(filename=file_path)
    sheet = wb.active
    val_list = []
    n = start_reading_from
    if type == 'name':
        while sheet[column_name_of_reading + str(n)].value:
            val_list.append(sheet[column_name_of_reading + str(n)].value)
            n += 1
    if type == 'description':
        while sheet[column_name_of_reading + str(n)].value:
            val_list.append(sheet['I' + str(n)].value)
            n += 1



    return val_list

def read_cell_xlsx(file_path, cell_name_of_reading):
    wb = openpyxl.load_workbook(filename=file_path)
    sheet = wb.active
    val = sheet[cell_name_of_reading].value
    return val


def write_xlsx(file_path, num_column_to_writing, text_to_write):
    wb = openpyxl.load_workbook(filename=file_path)
    sheet = wb.active
    sheet[num_column_to_writing] = text_to_write
    wb.save(file_path)


def files_find(dir_path, file_list, path1):
    fw = os.walk(dir_path)
    files_tree_list = []

    for file in fw:
        files_tree_list.append(file)

    exist_files_list = [[], []]

    for file_name in file_list:
        file_exist = False

        for file in files_tree_list:
            if file[2]:  # if directory contains files
                for n in file[2]:
                    if n == file_name:  # if filename from list find in directory
                        exist_files_list[0].append(n)  # write in list filename
                        exist_files_list[1].append(file[0])  # write in list file path
                        # print(file[0] + '\\'+file_name)
                        file_exist = True
                        # print("File " + str(file_name) + ' exist in path New_reports')
                        break
                if file_exist == True:
                    break
            if file_exist == True:
                break

        if file_exist == False:
            print("File " + str(file_name) + ' not exist in path')
            # print('I will remove:  '+path1+file_name)
            # os.remove(path1+file_name)

    return exist_files_list  # return list[['filename'], ['file path']]


def dir_creating(dir_path, dir_name):
    os.mkdir(dir_path+dir_name)

# copy("D:\\MyCode\\macroschecker\\Data\\Info.xlsx", 'D:\\MyCode\\macroschecker\\Data\\MacrosPackages\\Info.xlsx')
def path_edit(path):
    path.replace('\\', '\\\\')
    return path

# def rar_create(path):
#     print(path)
#     rarfile.
#
# rar_create("D:\\MyCode\\macroschecker\\Data\\testFolder\\test.rar")

def rename_file(filepath, filename):
    name, file_extension = os.path.splitext(filepath+filename)
    os.rename(filepath + filename, filepath + 'forsign' + file_extension)
    # print(filepath + 'forsign' + file_extnsion)

# Копирование Директорий

def copyFilesForSignFromFile(pathFromFileList, pathTo):

    for path in pathFromFileList:
        print(path)
        namefDir = os.path.basename(os.path.split(path)[0])
        shutil.copytree(os.path.split(path)[0], pathTo+namefDir)
        print(os.path.split(path)[0] + " was copied!")


# Переименовка .msg
def rename_1(path):
    fw = os.walk(path)
    tree = []

    for file in fw:
        # print(file[2])
        tree.append(file[2])
    # n = 0
    for filename in tree[0]:

        # name = str(n)
        # print(name)
        os.rename(path + filename, path + "d"+filename)
        # n += 1

